package main

import (
	mysql "github.com/Philio/GoMySQL"
	"os"
	"log"
)

var dbPool chan *mysql.Client

func initDb() {
	log.SetFlags(log.Ltime | log.Lshortfile)

	const dbPoolSize = 4
	dbPool = make(chan *mysql.Client, 4)

	for i := 0; i < dbPoolSize; i++ {
		db, err := mysql.DialTCP("localhost", "audio", "audio", "audio")
		if err != nil {
			log.Panicln(err)
		}
		db.Reconnect = true
		dbPool <- db
	}
}

func prepare(db *mysql.Client, sql string, params ...interface{}) (*mysql.Statement, os.Error) {
	query, err := db.Prepare(sql)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	err = query.BindParams(params...)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	err = query.Execute()
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return query, err
}

func queryInt(db *mysql.Client, sql string, params ...interface{}) (int, os.Error) {
	query, err := prepare(db, sql, params...)
	if err != nil {
		return 0, err
	}
	var result int
	query.BindResult(&result)
	eof, err := query.Fetch()
	if err != nil {
		log.Println(err)
		return result, err
	} else if eof {
		return result, os.EOF
	}
	err = query.FreeResult()
	if err != nil {
		log.Println(err)
		return result, err
	}
	return result, err
}

// given an id ('abcd1234'), return the pid (1)
func getpid(db *mysql.Client, id string) int {
	pid, err := queryInt(db, "SELECT `pid` FROM `playlist` WHERE `id` = ?", id)
	if err != nil {
		return -1
	}
	return pid
}
