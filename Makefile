include $(GOROOT)/src/Make.inc

TARG=aa
GOFILES=main.go db.go updates.go mixpanel.go
pkgdir=$(QUOTED_GOROOT)/pkg/$(GOOS)_$(GOARCH)
PREREQ=$(pkgdir)/github.com/Philio/GoMySQL.a

include $(GOROOT)/src/Make.cmd
