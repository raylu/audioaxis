package main

import (
	"http"
	"encoding/base64"
	"json"
	"log"
	"url"
)

func init() {
	log.SetFlags(log.Ltime | log.Lshortfile)
}

func track(event, ip, id, song, user string) {
	const token = "643bb718adc5d66de18617b4f0bf3489"

	data := make(map[string]interface{})
	data["event"] = event
	properties := make(map[string]string)
	properties["token"] = token
	properties["ip"] = ip
	properties["id"] = id
	if song != "" {
		properties["song"] = song
	}
	if user != "" {
		properties["user"] = user
		if user != "anonymous" {
			properties["mp_name_tag"] = user
		}
	}
	data["properties"] = properties

	dataJSON, err := json.Marshal(data)
	if err != nil {
		log.Println(err)
		return
	}

	values := make(url.Values)
	values.Add("data", base64.StdEncoding.EncodeToString(dataJSON))
	r, err := http.PostForm("https://api.mixpanel.com/track/", values)
	if err == nil {
		r.Body.Close()
	}
}
